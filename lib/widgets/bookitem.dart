import 'package:flutter/material.dart';
import '../model/book_model.dart';

class BookItem extends StatelessWidget {
  final Book book;

  const BookItem({required this.book});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.network(book.image),
      title: Text(book.title),
      subtitle: Text(book.category.name),
      onTap: () {
        Navigator.pushNamed(context, '/book-detail', arguments: {'book': book});
      },
    );
  }
}
