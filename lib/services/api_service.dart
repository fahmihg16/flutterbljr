// import 'dart:convert';
// import 'package:http/http.dart' as http;

// import '../utils/share_preferences.dart';

// class ApiService {
//   static const String baseUrl = 'http://10.0.2.2:8000';

//   Future<Map<String, dynamic>> memberlogin(
//       String username, String password) async {
//     const String apiUrl =
//         '$baseUrl/api/member-login/'; // Sesuaikan dengan endpoint login Anda

//     try {
//       final response = await http.post(
//         Uri.parse(apiUrl),
//         headers: <String, String>{
//           'Content-Type': 'application/json; charset=UTF-8',
//         },
//         body: jsonEncode(
//           <String, String>{
//             'username': username,
//             'password': password,
//           },
//         ),
//       );

//       if (response.statusCode == 200) {
//         // Parsing dan mengembalikan data dari respons API
//         Map<String, dynamic> data = jsonDecode(response.body);
//         final String accessToken = data['your_received_access_token'];
//         await SharedPreferencesUtils.saveAccessToken(accessToken);
//         return data;
//       } else {
//         // Handling jika login gagal
//         return {'error': 'Login failed'};
//       }
//     } catch (error) {
//       // Handling jika terjadi kesalahan selama panggilan API
//       return {'error': 'An error occurred during the API call'};
//     }
//   }

//   Future<Map<String, dynamic>> librarianlogin(
//       String username, String password) async {
//     const String apiUrl =
//         '$baseUrl/api/librarian-login'; // Sesuaikan dengan endpoint login Anda

//     try {
//       final response = await http.post(
//         Uri.parse(apiUrl),
//         headers: <String, String>{
//           'Content-Type': 'application/json; charset=UTF-8',
//         },
//         body: jsonEncode(
//           <String, String>{
//             'username': username,
//             'password': password,
//           },
//         ),
//       );

//       if (response.statusCode == 200) {
//         // Parsing dan mengembalikan data dari respons API
//         Map<String, dynamic> data = jsonDecode(response.body);
//         return data;
//       } else {
//         // Handling jika login gagal
//         return {'error': 'Login failed'};
//       }
//     } catch (error) {
//       // Handling jika terjadi kesalahan selama panggilan API
//       return {'error': 'An error occurred during the API call'};
//     }
//   }

//   Future<String?> getAccessToken() async {
//     return SharedPreferencesUtils.getAccessToken();
//   }

//   Future<void> saveAccessToken(String token) async {
//     return SharedPreferencesUtils.saveAccessToken(token);
//   }

//   Future<void> removeAccessToken() async {
//     return SharedPreferencesUtils.removeAccessToken();
//   }
// }


// // Map<String, dynamic> _handleResponse(http.Response response) {
// //     if (response.statusCode == 200) {
// //       return jsonDecode(response.body);
// //     } else {
// //       throw Exception('Failed to load data');
// //     }
// //   }