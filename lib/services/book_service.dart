// book_service.dart
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../model/book_model.dart';
import '../model/category_model.dart';

class BookService {
  static const String baseUrl = "http://10.0.2.2:8000/api";

  static Future<List<Book>> getBooks(String accessToken) async {
    final response = await http.get(
      Uri.parse("$baseUrl/auth/book/"),
      headers: {"Authorization": "Bearer $accessToken"},
    );

    if (response.statusCode == 200) {
      final List<dynamic> data = json.decode(response.body);
      List<Book> books = data.map((item) => Book.fromJson(item)).toList();
      return books;
    } else {
      throw Exception('Failed to load categories');
    }
  }

//   static Future<Map<String, dynamic>> getCategories(String accessToken) async {
//     final response = await http.get(
//       Uri.parse("$baseUrl/category/"),
//       headers: {"Authorization": "Bearer $accessToken"},
//     );

//     if (response.statusCode == 200) {
//       final Map<String, dynamic> data = json.decode(response.body);
//       return data;
//     } else {
//       throw Exception('Failed to load books');
//     }
//   }

//   static Future<Map<String, dynamic>> getBookByCategory(
//       String category, String accessToken) async {
//     final response = await http.get(
//       Uri.parse("$baseUrl/book/by-category/$category"),
//       headers: {"Authorization": "Bearer $accessToken"},
//     );

//     if (response.statusCode == 200) {
//       final Map<String, dynamic> data = json.decode(response.body);
//       return data;
//     } else {
//       throw Exception('Failed to load books');
//     }
//   }

//   // Future<List<Category>> getCategories() async {
//   //   final response = await http.get(Uri.parse("$baseUrl/category"), headers: {
//   //     'Authorization': 'Bearer
//   //   });

//   //   if (response.statusCode == 200) {
//   //     List<dynamic> data = json.decode(response.body);
//   //     List<Category> categories =
//   //         data.map((json) => Category.fromJson(json)).toList();

//   //     return categories;
//   //   } else {
//   //     throw Exception('Failed to load categories');
//   //   }
//   // }
// }

// // class BookService {
// //   static const String apiUrl = 'http://10.0.2.2:8000/api';

// //   static Future<dynamic> getBooks() async {
// //     SharedPreferences prefs = await SharedPreferences.getInstance();
// //     final String accessToken = prefs.getString('accessToken') ?? '';

// //     final Map<String, String> headers = {
// //       'Authorization': 'Bearer $accessToken',
// //     };

// //     final response = await http.get(
// //       Uri.parse('$apiUrl/book'),
// //       headers: headers,
// //     );

// //     if (response.statusCode == 200) {
// //       final List<dynamic> jsonData = json.decode(response.body)['results'];
// //       return jsonData.map((data) => Book.fromJson(data)).toList();
// //     } else {
// //       print('Failed to load data: ${response.statusCode}');
// //     }
// //   }

// //   static Future<List<Book>> getBooksByCategory(String category) async {
// //     SharedPreferences prefs = await SharedPreferences.getInstance();
// //     final String accessToken = prefs.getString('accessToken') ?? '';

// //     final Map<String, String> headers = {
// //       'Authorization': 'Bearer $accessToken',
// //     };

// //     final response = await http.get(
// //       Uri.parse('$apiUrl/by-category/$category/'),
// //       headers: headers,
// //     );

// //     if (response.statusCode == 200) {
// //       final List<dynamic> jsonData = json.decode(response.body)['results'];
// //       return jsonData.map((data) => Book.fromJson(data)).toList();
// //     } else {
// //       throw Exception('Failed to load data: ${response.statusCode}');
// //     }
// //   }

// //   static Future<List<Category>> getCategories() async {
// //     SharedPreferences prefs = await SharedPreferences.getInstance();
// //     final String accessToken = prefs.getString('accessToken') ?? '';

// //     final Map<String, String> headers = {
// //       'Authorization': 'Bearer $accessToken',
// //     };

// //     final response = await http.get(
// //       Uri.parse('$apiUrl/category/'),
// //       headers: headers,
// //     );

// //     if (response.statusCode == 200) {
// //       final List<dynamic> jsonData = json.decode(response.body)['results'];
// //       return jsonData.map((data) => Category.fromJson(data)).toList();
// //     } else {
// //       throw Exception('Failed to load categories: ${response.statusCode}');
// //     }
// //   }
}
