import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../utils/share_preferences.dart';

enum UserRole { member, librarian }

class AuthenticationService {
  static const String apiUrl =
      'http://10.0.2.2:8000/api/member-login/'; // Replace with your API endpoint

  static Future<String?> login(String username, String password) async {
    try {
      final response = await http.post(
        Uri.parse(apiUrl),
        body: {'username': username, 'password': password},
      );

      if (response.statusCode == 200) {
        final Map<String, dynamic> data = json.decode(response.body);
        final String accessToken = data['your_received_access_token'];
        await SharedPreferencesUtils.saveAccessToken(accessToken);
        return accessToken;
      } else {
        throw Exception('Authentication failed');
      }
    } catch (e) {
      print('Error during login: $e');
      throw Exception('Failed to connect to the server');
    }
  }

  static Future<void> logout() async {
    await SharedPreferencesUtils.removeAccessToken();
  }

  static Future<bool> isAuthenticated() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('access_token') != null;
  }

  static Future<UserRole?> getUserRole() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('access_token');

    // Jika token tidak ada, kembalikan null
    if (token == null) return null;

    if (token.contains('member')) {
      return UserRole.member;
    } else if (token.contains('librarian')) {
      return UserRole.librarian;
    }
    return null;
  }
}

class AuthenticationException implements Exception {
  final String message;

  AuthenticationException(this.message);
}
