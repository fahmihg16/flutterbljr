import 'dart:convert';

import 'package:http/http.dart' as http;

class UserService {
  final String baseUrl;
  final String authToken;

  UserService({required this.baseUrl, required this.authToken});

  Future<void> updateUserProfile(String userId, String newEmail,
      String newFirstName, String newLastName) async {
    try {
      final response = await http.put(
        Uri.parse('$baseUrl/users/$userId'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $authToken', // Header autentikasi
        },
        body: jsonEncode(<String, String>{
          'email': newEmail,
          'first_name': newFirstName,
          'last_name': newLastName,
        }),
      );

      if (response.statusCode == 200) {
        // Perubahan berhasil disimpan
        print('User profile updated successfully.');
      } else {
        // Gagal menyimpan perubahan
        print('Failed to update user profile.');
        throw Exception('Failed to update user profile.');
      }
    } catch (e) {
      print('Error: $e');
      throw Exception('Error occurred while updating user profile.');
    }
  }
}
