import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import 'bookloandetail_page.dart';

class BookLoanPage extends StatefulWidget {
  @override
  _BookLoanPageState createState() => _BookLoanPageState();
}

class _BookLoanPageState extends State<BookLoanPage> {
  late ScrollController _scrollController;
  late List<Map<String, dynamic>> _bookLoans;
  late String? _nextPageUrl;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController()..addListener(_scrollListener);
    _bookLoans = [];
    _nextPageUrl = 'http://10.0.2.2:8000/api/bookloan/';
    _loadData();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      // Load more data when scrolled to the bottom
      _loadData();
    }
  }

  Future<void> _loadData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') ?? '';

    final response = await http.get(
      Uri.parse(_nextPageUrl!),
      headers: {
        "Authorization": "Bearer $accessToken",
      },
    );

    if (response.statusCode == 200) {
      final jsonData = jsonDecode(response.body);
      final List<dynamic> data = jsonData['results'];
      setState(() {
        _bookLoans.addAll(data.cast<Map<String, dynamic>>());
        _nextPageUrl = jsonData['next'];
      });
    } else if (response.statusCode == 403) {
      throw Exception(
          'Access denied. Only librarians are allowed to view book loans.');
    } else {
      throw Exception('Failed to load book loans');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Book Loans'),
      ),
      body: ListView.builder(
        controller: _scrollController,
        itemCount: _bookLoans.length,
        itemBuilder: (context, index) {
          final loan = _bookLoans[index];
          return Card(
            child: ListTile(
              leading: Container(
                width: 50,
                child: Image.network(
                  loan['book_image_url'],
                  fit: BoxFit.cover,
                ),
              ),
              title: Text(loan['book_title']),
              trailing: Text('Returned: ${loan['is_returned'] ? 'Yes' : 'No'}'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Loan Member: ${loan['member_name']}'),
                  // Tampilkan status isReturned
                ],
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => BookLoanDetailPage(loan: loan),
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }
}
