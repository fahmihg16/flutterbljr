import 'package:belajar_flutter/page/dashboard_page.dart';
import 'package:flutter/material.dart';
import 'package:belajar_flutter/model/book_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class BookDetailScreenPage extends StatelessWidget {
  final Book book;

  const BookDetailScreenPage({Key? key, required this.book}) : super(key: key);

  Future<void> _borrowBook(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') ?? '';

    // Tampilkan dialog untuk meminta input tanggal jatuh tempo
    DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2101),
    );

    // Jika pengguna memilih tanggal, kirim permintaan pinjam buku
    if (pickedDate != null) {
      // Konversi tanggal ke format "YYYY-MM-DD"
      String formattedDate =
          "${pickedDate.year}-${pickedDate.month.toString().padLeft(2, '0')}-${pickedDate.day.toString().padLeft(2, '0')}";

      var payload = {
        "due_date": formattedDate, // Menggunakan format "YYYY-MM-DD"
      };

      // Kirim permintaan pinjam buku ke backend
      var response = await http.post(
        Uri.parse("http://10.0.2.2:8000/api/member/book-loan/${book.id}/"),
        headers: {
          "Authorization": "Bearer $accessToken",
          "Content-Type": "application/json",
        },
        body: jsonEncode(payload), // Encode payload data sebagai JSON
      );

      // Periksa status kode respons
      if (response.statusCode == 400) {
        // Buku sudah dipinjam, tampilkan Snackbar dengan pesan dari respons
        Map<String, dynamic> responseData = json.decode(response.body);
        String message = responseData['message'];
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(message),
            duration: Duration(seconds: 2),
          ),
        );
      } else if (response.statusCode == 201) {
        // Buku berhasil dipinjam, tampilkan Snackbar sukses
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Buku berhasil dipinjam'),
            duration: Duration(seconds: 2),
          ),
        );
      } else {
        // Respons tidak diharapkan, tampilkan Snackbar dengan pesan default
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Gagal meminjam buku'),
            duration: Duration(seconds: 2),
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(book.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.network(
              book.image,
              width: 200,
              height: 200,
              fit: BoxFit.cover,
            ),
            SizedBox(height: 16),
            Text(
              'Author: ${book.author}',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 8),
            Text(
              'Category: ${book.category.name}',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 8),
            Text(
              'Description: ${book.datePublish}',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 8),
            Text(
              'Published Year: ${book.isbn}',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 16),
            ElevatedButton(
              onPressed: () {
                _borrowBook(
                    context); // Panggil fungsi pinjam buku saat tombol ditekan
              },
              child: Text('Pinjam Buku'),
            ),
          ],
        ),
      ),
    );
  }
}
