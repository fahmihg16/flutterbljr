// edit_profile_page.dart

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class EditProfilePage extends StatelessWidget {
  late TextEditingController _emailController = TextEditingController();
  late TextEditingController _firstNameController = TextEditingController();
  late TextEditingController _lastNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Profile'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextFormField(
              controller: _emailController,
              decoration: InputDecoration(labelText: 'Email'),
            ),
            TextFormField(
              controller: _firstNameController,
              decoration: InputDecoration(labelText: 'First Name'),
            ),
            TextFormField(
              controller: _lastNameController,
              decoration: InputDecoration(labelText: 'Last Name'),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () => _updateProfile(context),
              child: Text('Save'),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _updateProfile(BuildContext context) async {
    final updatedData = {
      'email': _emailController.text,
      'first_name': _firstNameController.text,
      'last_name': _lastNameController.text,
    };

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') ?? '';

    final response = await http.put(
      Uri.parse(
          'http://10.0.2.2:8000/api/change-profile/'), // Sesuaikan dengan endpoint API Anda
      body: updatedData,
      headers: {
        "Authorization": "Bearer $accessToken"
      }, // Tambahkan header autentikasi
    );

    if (response.statusCode == 200) {
      // Perubahan profil berhasil
      // Tambahkan logika penanganan berhasil di sini
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Profile updated successfully'),
          duration: Duration(seconds: 2),
        ),
      );
    } else {
      // Perubahan profil gagal
      // Tambahkan logika penanganan kesalahan di sini
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Failed to update profile'),
          duration: Duration(seconds: 2),
        ),
      );
    }
  }
}
