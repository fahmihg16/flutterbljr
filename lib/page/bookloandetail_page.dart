import 'package:flutter/material.dart';

class BookLoanDetailPage extends StatelessWidget {
  final Map<String, dynamic> loan;

  BookLoanDetailPage({required this.loan});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Book Loan Detail'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Book Title: ${loan['book_title']}',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 8),
            Image.network(
              loan['book_image_url'],
              height: 200,
              width: 200,
              fit: BoxFit.cover,
            ),
            SizedBox(height: 8),
            Text('Loan Member: ${loan['member_name']}'),
            SizedBox(height: 8),
            Text('Loan Date: ${loan['loan_date']}'),
            SizedBox(height: 8),
            Text('Due Date: ${loan['due_date']}'),
            SizedBox(height: 8),
            Text('Return Date: ${loan['return_date'] ?? 'Not returned'}'),
            SizedBox(height: 8),
            Text('Is Returned: ${loan['is_returned'] ? 'Yes' : 'No'}'),
          ],
        ),
      ),
    );
  }
}
