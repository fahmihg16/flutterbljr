import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class OverdueBookLoanPage extends StatefulWidget {
  @override
  _OverdueBookLoanPageState createState() => _OverdueBookLoanPageState();
}

class _OverdueBookLoanPageState extends State<OverdueBookLoanPage> {
  List<dynamic> _overdueBookLoans = [];
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    _fetchOverdueBookLoans();
  }

  Future<void> _fetchOverdueBookLoans() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') ?? '';

    if (accessToken == null) {
      // Token tidak tersedia, lakukan penanganan sesuai kebutuhan aplikasi Anda
      return;
    }

    final url = Uri.parse('http://10.0.2.2:8000/api/overdue-books/');
    final response = await http.get(
      url,
      headers: <String, String>{
        'Authorization': 'Bearer $accessToken',
      },
    );

    if (response.statusCode == 200) {
      setState(() {
        _overdueBookLoans = json.decode(response.body);
        _isLoading = false;
      });
    } else {
      // Handle error
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Overdue Book Loans'),
      ),
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : _overdueBookLoans.isEmpty
              ? Center(child: Text('No overdue book loans found'))
              : ListView.builder(
                  itemCount: _overdueBookLoans.length,
                  itemBuilder: (context, index) {
                    final loan = _overdueBookLoans[index];
                    return ListTile(
                      title: Text(loan['book_title']),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Loan Date: ${loan['loan_date']}'),
                          Text('Due Date: ${loan['due_date']}'),
                          Text('Member: ${loan['member_name']}'),
                          Text('Overdue Days: ${loan['days_over']}'),
                        ],
                      ),
                      leading: loan['book_image_url'] != null
                          ? Image.network(
                              loan['book_image_url'],
                              width: 50,
                              height: 50,
                              fit: BoxFit.cover,
                            )
                          : Container(
                              // Gunakan Container untuk menetapkan ukuran Placeholder
                              width: 50,
                              height: 50,
                              child: Placeholder(),
                            ),
                    );
                  },
                ),
    );
  }
}
