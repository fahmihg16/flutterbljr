import 'package:flutter/material.dart';
import '../model/book_model.dart';

class BookDetailPage extends StatelessWidget {
  final Book book; // Definisi parameter book

  // Konstruktor
  const BookDetailPage({Key? key, required this.book}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Implementasi tampilan halaman detail buku di sini
    return Scaffold(
      appBar: AppBar(
        title: Text('Book Detail'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Title: ${book.title}'),
            Text('Author: ${book.author}'),
            Image.network(book.image)
            // Tambahkan detail buku lainnya sesuai kebutuhan
          ],
        ),
      ),
    );
  }
}
