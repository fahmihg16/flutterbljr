import 'package:belajar_flutter/model/bookloanmember_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class BookLoanMemberPage extends StatefulWidget {
  @override
  _BookLoanMemberPageState createState() => _BookLoanMemberPageState();
}

class _BookLoanMemberPageState extends State<BookLoanMemberPage> {
  late Future<List<BookLoanMember>> _BookLoanMembers;

  @override
  void initState() {
    super.initState();
    _BookLoanMembers = fetchBookLoanMembers();
  }

  Future<List<BookLoanMember>> fetchBookLoanMembers() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') ?? '';

    final response = await http.get(
      Uri.parse('http://10.0.2.2:8000/api/member/list-book-loan/'),
      headers: {
        "Authorization": "Bearer $accessToken",
      },
    );

    if (response.statusCode == 200) {
      final List<dynamic> data = jsonDecode(response.body);

      // Filter hanya buku yang belum dikembalikan
      List<BookLoanMember> notReturnedBooks = data
          .map((item) => BookLoanMember.fromJson(item))
          .where((book) => !book.isReturned)
          .toList();

      return notReturnedBooks;

      // return data.map((item) => BookLoanMember.fromJson(item)).toList();
    } else {
      throw Exception('Failed to load borrowed books');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Loaned Books'),
      ),
      body: FutureBuilder<List<BookLoanMember>>(
        future: _BookLoanMembers,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else {
            return ListView.builder(
              itemCount: snapshot.data!.length,
              itemBuilder: (context, index) {
                BookLoanMember book = snapshot.data![index];
                return ListTile(
                  leading: Container(
                    width: 50, // Sesuaikan dengan lebar yang diinginkan
                    child: Image.network(
                      book.imageUrl,
                      fit: BoxFit.cover,
                    ),
                  ),
                  title: Text(book.title),
                  trailing: Text('Returned: ${book.isReturned ? 'Yes' : 'No'}'),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Due Date: ${book.dueDate}'),
                      // Tampilkan status isReturned
                    ],
                  ),
                );
              },
            );
          }
        },
      ),
    );
  }
}
