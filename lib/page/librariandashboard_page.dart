import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../model/book_model.dart';
import '../model/category_model.dart';
import '../model/user_model.dart';
import '../services/auth_service.dart';
import 'bookdetail_page.dart';
import 'bookdetailscreen_page.dart';
import 'bookloan_page.dart';
import 'booksbycategory_page.dart';
import 'changepassword_page.dart';
import 'editprofile_page.dart';
import 'home_page.dart';
import 'nearoutstandingbookloan_page.dart';
import 'overdue_page.dart';

class LibrarianDashboard extends StatefulWidget {
  @override
  _LibrarianDashboardState createState() => _LibrarianDashboardState();
}

class _LibrarianDashboardState extends State<LibrarianDashboard> {
  late List<Book> _books = [];
  late List<Book> _filteredBooks = [];
  late List<Category> _categories = [];
  late String _selectedCategory = 'All';
  TextEditingController _searchController = TextEditingController();
  late User _currentUser;
  bool _loading = false;
  int _page = 1;

  @override
  void initState() {
    super.initState();
    _loadBooks();
    _loadCategories();
    _loadCurrentUser();
  }

  Future<void> _loadCurrentUser() async {
    // Ambil informasi pengguna dari Shared Preferences atau sumber lainnya
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String currentUserJson = prefs.getString('currentUser') ?? '';
    if (currentUserJson.isNotEmpty) {
      _currentUser = User.fromJson(jsonDecode(currentUserJson));
    }
  }

  // Future<List<Book>> _loadBooks() async
  Future<void> _loadBooks({bool refresh = false}) async {
    if (!refresh) {
      setState(() {
        _loading = true;
      });
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') ?? '';

    final response = await http.get(
      Uri.parse("http://10.0.2.2:8000/api/auth/book/?page=$_page"),
      headers: {"Authorization": "Bearer $accessToken"},
    );

    if (response.statusCode == 200) {
      final List<dynamic> data = json.decode(response.body)['results'];
      List<Book> books = data.map((item) => Book.fromJson(item)).toList();
      setState(() {
        if (refresh) {
          _books = books;
          _filteredBooks = _books;
        } else {
          _books.addAll(books);
          _filteredBooks = _books;
        }
        _loading = false;
        _page++;
      });
    } else {
      throw Exception('Failed to load books');
    }
  }

  Future<void> _loadCategories() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') ?? '';

    final response = await http.get(
      Uri.parse("http://10.0.2.2:8000/api/category/"),
      headers: {"Authorization": "Bearer $accessToken"},
    );

    if (response.statusCode == 200) {
      print('Categories: $_categories');
      final List<dynamic> data = json.decode(response.body)['results'];
      List<Category> categories =
          data.map((item) => Category.fromJson(item)).toList();
      setState(() {
        _categories = categories;
      });
    } else {
      throw Exception('Failed to load categories');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 230, 114, 20),
        title: Text('Librarian Dashboard'),
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: () {
              AuthenticationService.logout();
              _logout(context);
            },
          )
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: _searchController,
              decoration: InputDecoration(
                labelText: 'Search by title',
                prefixIcon: Icon(Icons.search),
              ),
              onChanged: (value) {
                _filterBooks(value);
              },
            ),
          ),
          Expanded(
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (!_loading &&
                    scrollInfo.metrics.pixels ==
                        scrollInfo.metrics.maxScrollExtent) {
                  _loadBooks();
                  return true;
                }
                return false;
              },
              child: _buildBookList(),
            ),
          ),
          if (_loading)
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CircularProgressIndicator(),
            ),
          // Dropdown untuk memilih kategori
          DropdownButton<String>(
            value: _selectedCategory,
            onChanged: (String? category) {
              setState(() {
                _selectedCategory = category!;
              });
              if (_selectedCategory != 'All') {
                _navigateToBooksByCategoryPage(_selectedCategory);
              } else {
                setState(() {
                  _filteredBooks = _books;
                });
              }
            },
            items: [
              DropdownMenuItem<String>(
                value: 'All',
                child: Text('All'),
              ),
              ..._categories.map<DropdownMenuItem<String>>((Category category) {
                return DropdownMenuItem<String>(
                  value: category.name,
                  child: Text(category.name),
                );
              }).toList(),
            ],
          ),
        ],
      ),
      drawer: _buildDrawer(context),
    );
  }

  Widget _buildDrawer(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 230, 114, 20),
            ),
            child: Text(
              'Menu',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
          ListTile(
            title: Text('Dashboard'),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: Text('Book Loan'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => BookLoanPage()),
              );
            },
          ),
          ListTile(
            title: Text('Near Outstanding Book Loan'),
            onTap: () {
              Navigator.pop(context); // Close the drawer
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => NearOutstandingBookLoanPage(),
                ),
              );
            },
          ),
          ListTile(
            title: Text('Overdue Book Loan'),
            onTap: () {
              Navigator.pop(context); // Close the drawer
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => OverdueBookLoanPage(),
                ),
              );
            },
          ),
          ListTile(
            title: Text('Edit Profile'),
            onTap: () {
              Navigator.pop(context); // Tutup drawer
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => EditProfilePage()),
              );
            },
          ),
          ListTile(
            title: Text('Change Password'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ChangePasswordPage()),
              );
            },
          ),
          // Tambahkan item menu lain di sini
        ],
      ),
    );
  }

  Widget _buildBookList() {
    if (_filteredBooks.isEmpty) {
      return Center(child: CircularProgressIndicator());
    } else {
      return ListView.builder(
        itemCount: _filteredBooks.length,
        itemBuilder: (context, index) {
          var book = _filteredBooks[index];
          return ListTile(
            title: Text(book.title),
            subtitle: Text(book.category.name),
            leading: Image.network(
              book.image,
              width: 50,
              height: 50,
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => BookDetailScreenLibPage(book: book),
                ),
              );
            },
          );
        },
      );
    }
  }

  void _navigateToBooksByCategoryPage(String category) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') ?? '';

    try {
      final response = await http.get(
        Uri.parse("http://10.0.2.2:8000/api/book/by-category/$category/"),
        headers: {"Authorization": "Bearer $accessToken"},
      );

      if (response.statusCode == 200) {
        final List<dynamic> data = json.decode(response.body)['results'];
        List<Book> books = data.map((item) => Book.fromJson(item)).toList();

        print('Books by category: $books');

        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => BooksByCategoryPage(
              category: category,
              books: books,
            ),
          ),
        );
      } else {
        throw Exception('Failed to load books by category');
      }
    } catch (e) {
      print('Error: $e');
      // Tambahkan penanganan kesalahan sesuai kebutuhan, misalnya menampilkan snackbar atau dialog
    }
  }

  void _filterBooks(String query) {
    setState(() {
      if (query.isEmpty) {
        _filteredBooks = _books;
      } else {
        _filteredBooks = _books
            .where((book) =>
                book.title.toLowerCase().contains(query.toLowerCase()))
            .toList();
      }
    });
  }

  void _filterBooksByCategory(String category) {
    setState(() {
      if (category == 'All') {
        _filteredBooks = _books;
      } else {
        _filteredBooks =
            _books.where((book) => book.category == category).toList();
      }
    });
  }

  Future<void> _logout(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => HomePage(), // Ganti dengan halaman login/register
      ),
    );
  }
}
