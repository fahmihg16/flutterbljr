import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class NearOutstandingBookLoanPage extends StatefulWidget {
  @override
  _NearOutstandingBookLoanPageState createState() =>
      _NearOutstandingBookLoanPageState();
}

class _NearOutstandingBookLoanPageState
    extends State<NearOutstandingBookLoanPage> {
  List<dynamic> _nearOutstandingBookLoans = [];
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    _fetchNearOutstandingBookLoans();
  }

  Future<void> _fetchNearOutstandingBookLoans() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') ?? '';

    if (accessToken == null) {
      // Token tidak tersedia, lakukan penanganan sesuai kebutuhan aplikasi Anda
      return;
    }

    final url = Uri.parse('http://10.0.2.2:8000/api/near-outstanding-books/');
    final response = await http.get(
      url,
      headers: <String, String>{
        'Authorization': 'Bearer $accessToken',
      },
    );

    if (response.statusCode == 200) {
      setState(() {
        _nearOutstandingBookLoans = json.decode(response.body);
        _isLoading = false;
      });
    } else {
      // Handle error
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Near Outstanding Book Loans'),
      ),
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : _nearOutstandingBookLoans.isEmpty
              ? Center(child: Text('No near outstanding book loans found'))
              : ListView.builder(
                  itemCount: _nearOutstandingBookLoans.length,
                  itemBuilder: (context, index) {
                    final loan = _nearOutstandingBookLoans[index];
                    return ListTile(
                      title: Text(loan['book_title']),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Loan Date: ${loan['loan_date']}'),
                          Text('Due Date: ${loan['due_date']}'),
                          Text('Member: ${loan['member_name']}'),
                          Text('Days Left: ${loan['days_left']}'),
                        ],
                      ),
                      leading: loan['book_image_url'] != null
                          ? Image.network(
                              loan['book_image_url'],
                              width: 50,
                              height: 50,
                              fit: BoxFit.cover,
                            )
                          : Container(
                              // Gunakan Container untuk menetapkan ukuran Placeholder
                              width: 50,
                              height: 50,
                              child: Placeholder(),
                            ),
                    );
                  },
                ),
    );
  }
}
