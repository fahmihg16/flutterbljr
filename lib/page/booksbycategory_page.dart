import 'package:flutter/material.dart';
import 'package:belajar_flutter/model/book_model.dart';

import 'bookdetail_page.dart';
import 'booklist_page.dart';

class BooksByCategoryPage extends StatelessWidget {
  final String category;
  final List<Book> books;

  const BooksByCategoryPage({
    Key? key,
    required this.category,
    required this.books,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Books - $category'),
      ),
      body: _buildBookList(books),
    );
  }

  Widget _buildBookList(List<Book> books) {
    if (books.isEmpty) {
      return Center(child: Text('No books available'));
    } else {
      return ListView.builder(
        itemCount: books.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(books[index].title),
            subtitle: Text(books[index].author),
            leading: Image.network(
              books[index].image,
              width: 50,
              height: 50,
            ),
            // Add logic to navigate to book detail page if needed
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      BookDetailScreenPage(book: books[index]),
                ),
              );
            },
          );
        },
      );
    }
  }
}
