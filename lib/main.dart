import 'package:belajar_flutter/page/booksbycategory_page.dart';
import 'package:belajar_flutter/services/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:belajar_flutter/page/login_page.dart';
import 'package:belajar_flutter/page/home_page.dart';
import 'package:belajar_flutter/page/register_page.dart';
import 'package:belajar_flutter/page/dashboard_page.dart';

import 'page/librariandashboard_page.dart';
import 'page/librarianlogin_page.dart';
import 'page/memberlogin_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/home', // Tentukan rute awal
        routes: {
          '/': (context) => AuthenticationWrapper(),
          '/dashboard': (context) => DashboardPage(),
          '/librarian_dashboard': (context) => LibrarianDashboard(),
          '/home': (context) => HomePage(),
          '/register': (context) => RegisterPage(),
          '/login/member': (context) =>
              MemberLoginPage(), // Ubah rute untuk login member
          '/login/librarian': (context) => LibrarianLoginPage(),
        });
  }
}

// class AuthenticationWrapper extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     final AuthenticationService _authenticationService =
//         AuthenticationService();

//     return FutureBuilder(
//       future: AuthenticationService.isAuthenticated(),
//       builder: (context, snapshot) {
//         if (snapshot.connectionState == ConnectionState.waiting) {
//           return CircularProgressIndicator();
//         } else {
//           if (snapshot.data == true) {
//             final userRole = _authenticationService
//                 .getUserRole(); // Misalnya, Anda memiliki metode untuk mendapatkan peran pengguna
//             if (userRole == UserRole.member) {
//               return DashboardPage(); // Pengguna adalah member, arahkan ke dashboard member
//             } else if (userRole == UserRole.librarian) {
//               return LibrarianDashboard(); // Pengguna adalah librarian, arahkan ke dashboard librarian
//             } else {
//               return Scaffold(
//                 body: Center(
//                   child: Text('Invalid user role!'),
//                 ),
//               );
//             }
//           } else {
//             return HomePage(); // Pengguna belum login, arahkan ke halaman utama
//           }
//         }
//       },
//     );
//   }
// }

class AuthenticationWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: AuthenticationService.isAuthenticated(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        } else {
          if (snapshot.data == true) {
            // Pengguna sudah login
            return FutureBuilder<UserRole?>(
              future: AuthenticationService.getUserRole(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicator();
                } else {
                  if (snapshot.hasData) {
                    // Peran pengguna sudah didapatkan
                    UserRole? userRole = snapshot.data;
                    if (userRole == UserRole.member) {
                      return DashboardPage(); // Navigasi ke dashboard member
                    } else if (userRole == UserRole.librarian) {
                      return LibrarianDashboard(); // Navigasi ke dashboard librarian
                    } else {
                      return HomePage(); // Peran tidak dikenali, kembali ke halaman login
                    }
                  } else {
                    return HomePage(); // Gagal mendapatkan peran, kembali ke halaman login
                  }
                }
              },
            );
          } else {
            return HomePage(); // Pengguna belum login, arahkan ke homepage
          }
        }
      },
    );
  }
}
