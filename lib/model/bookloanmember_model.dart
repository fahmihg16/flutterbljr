// borrowed_book.dart

class BookLoanMember {
  final String title;
  final String imageUrl;
  final String dueDate;
  final bool isReturned;

  BookLoanMember({
    required this.title,
    required this.imageUrl,
    required this.dueDate,
    required this.isReturned,
  });

  factory BookLoanMember.fromJson(Map<String, dynamic> json) {
    return BookLoanMember(
      title: json['book_info']['title'],
      imageUrl: json['book_info']['image'],
      dueDate: json['due_date'],
      isReturned: json['is_returned'],
    );
  }
}
