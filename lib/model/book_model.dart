// book_model.dart
import 'category_model.dart';

class Book {
  final int id;
  final String title;
  final String author;
  final String publisher;
  final Category category;
  final int datePublish;
  final String image;
  final String isbn;

  Book({
    required this.id,
    required this.title,
    required this.author,
    required this.publisher,
    required this.category,
    required this.datePublish,
    required this.image,
    required this.isbn,
  });

  factory Book.fromJson(Map<String, dynamic> json) {
    return Book(
      id: json['id'],
      title: json['title'],
      author: json['author'],
      publisher: json['publisher'],
      category: Category.fromJson(json['category']),
      datePublish: int.parse(json['date_publish'].toString()),
      image: json['image'],
      isbn: json['isbn'],
    );
  }
}
