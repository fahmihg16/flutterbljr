import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class BookLoan {
  final String bookTitle;
  final String bookImageUrl;
  final String memberName;
  final DateTime loanDate;
  final DateTime dueDate;
  final DateTime? returnDate;
  final bool isReturned;

  BookLoan({
    required this.bookTitle,
    required this.bookImageUrl,
    required this.memberName,
    required this.loanDate,
    required this.dueDate,
    required this.returnDate,
    required this.isReturned,
  });

  factory BookLoan.fromJson(Map<String, dynamic> json) {
    return BookLoan(
      bookTitle: json['book']['title'],
      bookImageUrl: json['book_image_url'],
      memberName: json['member']['name'],
      loanDate: DateTime.parse(json['loan_date']),
      dueDate: DateTime.parse(json['due_date']),
      returnDate: json['return_date'] != null
          ? DateTime.parse(json['return_date'])
          : null,
      isReturned: json['is_returned'],
    );
  }
}
