// token_provider.dart

import 'package:flutter/material.dart';

class TokenProvider with ChangeNotifier {
  String? _accessToken;

  String? get accessToken => _accessToken;

  setAccessToken(String? token) {
    _accessToken = token;
    notifyListeners();
  }
}
